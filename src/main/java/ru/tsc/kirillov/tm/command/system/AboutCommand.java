package ru.tsc.kirillov.tm.command.system;

public final class AboutCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "about";
    }

    @Override
    public String getArgument() {
        return "-a";
    }

    @Override
    public String getDescription() {
        return "Отображение информации о разработчике.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Разработчик: Кириллов Максим");
        System.out.println("E-mail: mkirillov@tsconsulting.com");
    }

}
