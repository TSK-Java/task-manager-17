package ru.tsc.kirillov.tm.command.system;

import ru.tsc.kirillov.tm.api.service.ICommandService;
import ru.tsc.kirillov.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    public ICommandService getCommandService() {
        return getServiceLocator().getCommandService();
    }

}
