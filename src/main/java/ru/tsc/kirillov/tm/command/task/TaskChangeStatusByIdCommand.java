package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.enumerated.Status;
import ru.tsc.kirillov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class TaskChangeStatusByIdCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-change-status-by-id";
    }

    @Override
    public String getDescription() {
        return "Изменить статус задачи по её ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Изменение статуса задачи по ID]");
        System.out.println("Введите ID задачи:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите статус:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        getTaskService().changeTaskStatusById(id, status);
    }

}
