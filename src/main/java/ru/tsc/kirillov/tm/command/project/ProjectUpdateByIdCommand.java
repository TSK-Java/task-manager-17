package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @Override
    public String getDescription() {
        return "Обновить проект по его ID.";
    }

    @Override
    public void execute() {
        System.out.println("[Обновление проекта по ID]");
        System.out.println("Введите ID проекта:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Введите имя:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Введите описание:");
        final String description = TerminalUtil.nextLine();
        getProjectService().updateById(id, name, description);
    }

}
