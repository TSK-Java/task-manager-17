package ru.tsc.kirillov.tm.command.system;

import ru.tsc.kirillov.tm.api.model.ICommand;
import ru.tsc.kirillov.tm.command.AbstractCommand;

import java.util.Collection;

public final class HelpCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getArgument() {
        return "-h";
    }

    @Override
    public String getDescription() {
        return "Отображение доступных команд.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand cmd: commands)
            System.out.println(cmd);
    }

}
