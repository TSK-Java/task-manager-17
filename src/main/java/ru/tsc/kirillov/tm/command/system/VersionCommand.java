package ru.tsc.kirillov.tm.command.system;

public final class VersionCommand extends AbstractSystemCommand {

    @Override
    public String getName() {
        return "version";
    }

    @Override
    public String getArgument() {
        return "-v";
    }

    @Override
    public String getDescription() {
        return "Отображение версии программы.";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.17.0");
    }

}
