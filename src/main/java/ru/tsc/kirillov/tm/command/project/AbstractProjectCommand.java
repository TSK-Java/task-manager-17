package ru.tsc.kirillov.tm.command.project;

import ru.tsc.kirillov.tm.api.service.IProjectService;
import ru.tsc.kirillov.tm.api.service.IProjectTaskService;
import ru.tsc.kirillov.tm.command.AbstractCommand;

public abstract class AbstractProjectCommand extends AbstractCommand {

    public IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    public IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

}
