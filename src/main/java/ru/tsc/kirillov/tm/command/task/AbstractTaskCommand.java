package ru.tsc.kirillov.tm.command.task;

import ru.tsc.kirillov.tm.api.service.ITaskService;
import ru.tsc.kirillov.tm.command.AbstractCommand;

public abstract class AbstractTaskCommand extends AbstractCommand {

    public ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

}
